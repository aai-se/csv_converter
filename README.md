#CSV Converter Package
This package offers options for converting a CSV file to JSON and XML to output as a string variable, or write to a file.

## Features
 * Generates single JAR file package that is importable in to an A2019 Control Room.
 * Existing Classes:
    * CSV to JSON
    * CSV to XML	
    


