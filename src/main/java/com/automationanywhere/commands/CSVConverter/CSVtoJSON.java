

package com.automationanywhere.commands.CSVConverter;
import com.automationanywhere.botcommand.data.Value;
import static com.automationanywhere.commandsdk.model.AttributeType.*;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.rules.CodeType;
import com.automationanywhere.commandsdk.annotations.rules.VariableType;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;

import java.io.File;
import java.io.FileWriter;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.dataformat.csv.*;
import shadow.org.apache.tools.ant.taskdefs.Input;

import static com.automationanywhere.commandsdk.model.AttributeType.FILE;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be displayable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "CSV to JSON", label = "CSV to JSON",
        node_label = "Convert CSV File to JSON", description = "CSV to JSON", icon = "myicon.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "Assign JSON-formatted Output to String", return_type = com.automationanywhere.commandsdk.model.DataType.STRING, return_required = true)

public class CSVtoJSON {
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Execute
    public StringValue action(
            @Idx(index = "1", type = AttributeType.FILE)
            @Pkg(label = "Input File", default_value_type = DataType.FILE)
            @NotEmpty String InputFile,

            @Idx(index = "2", type = AttributeType.FILE)
            @Pkg(label = "Output File .json Or .txt", default_value_type = DataType.FILE)
                    String OutputFile,

            @Idx(index = "3",  type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "3.1", pkg = @Pkg(label = "Overwrite", value = "Overwrite")),
                    @Idx.Option(index = "3.2", pkg = @Pkg(label = "Append", value = "Append")),
            }) @Pkg(label = "When writing to file", default_value = "Overwrite", default_value_type = DataType.STRING) String WriteMethod
    )
            throws IOException {
        if ("".equals(InputFile.trim())) {
            throw new BotCommandException(MESSAGES.getString("emptyInputFile", "InputFile"));
        }

        File input = new File(InputFile);

        // Read data from CSV file
        CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build();
        CsvMapper csvMapper = new CsvMapper();
        List<Object> readAll = csvMapper.readerFor(Map.class).with(csvSchema).readValues(input).readAll();
        ObjectMapper mapper = new ObjectMapper();
        //Write to output file if a path was provided
        if (OutputFile != null) {
            if ("".equals(OutputFile.trim())) {
                throw new BotCommandException(MESSAGES.getString("emptyOutputFile", "OutputFile"));
            }
            File output = new File(OutputFile);
            //if append radio is selected
            if (WriteMethod.equals("Append")) {
                FileWriter fr = new FileWriter(output, true);
                fr.write(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(readAll));
                fr.close();
            //if Overwrite is selected
            }else if(WriteMethod.equals("Overwrite")){
                mapper.writerWithDefaultPrettyPrinter().writeValue(output, readAll);
            }
        }
        return new StringValue(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(readAll));
    }
}
