package com.automationanywhere.commands.CSVConverter;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class CSVtoXMLCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(CSVtoXMLCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    CSVtoXML command = new CSVtoXML();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("InputFile") && parameters.get("InputFile") != null && parameters.get("InputFile").get() != null) {
      convertedParameters.put("InputFile", parameters.get("InputFile").get());
      if(!(convertedParameters.get("InputFile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InputFile", "String", parameters.get("InputFile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("InputFile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","InputFile"));
    }

    if(parameters.containsKey("OutputFile") && parameters.get("OutputFile") != null && parameters.get("OutputFile").get() != null) {
      convertedParameters.put("OutputFile", parameters.get("OutputFile").get());
      if(!(convertedParameters.get("OutputFile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFile", "String", parameters.get("OutputFile").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("WriteMethod") && parameters.get("WriteMethod") != null && parameters.get("WriteMethod").get() != null) {
      convertedParameters.put("WriteMethod", parameters.get("WriteMethod").get());
      if(!(convertedParameters.get("WriteMethod") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","WriteMethod", "String", parameters.get("WriteMethod").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("WriteMethod") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","WriteMethod"));
    }
    if(convertedParameters.get("WriteMethod") != null) {
      switch((String)convertedParameters.get("WriteMethod")) {
        case "Overwrite" : {

        } break;
        case "Append" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","WriteMethod"));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("InputFile"),(String)convertedParameters.get("OutputFile"),(String)convertedParameters.get("WriteMethod")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
